<?php
$filename = 'zip_data_split_1.csv';
$data = [];
// $output = [];
// for($i = 0; $i < $array_Line.length ; $i++){
//     $output = [$i];
//     var_dump($output);
// }
if (is_readable($filename) === TRUE) {
    $file =  fopen($filename, 'r');
    if ($file!== FALSE) {
        // while (($line = fgets($file)) !== FALSE) {
        //     // $line = htmlspecialchars($line,ENT_QUOTES,'UTF-8');
        //     $arrayLine = explode(",",$line);
        //     $data[] = $arrayLine;
        // }
        while (($arrayLine = fgetcsv($file)) !== FALSE) {
            $data[] = $arrayLine;
        }
        fclose($file);
    }
}    
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>9-11</title>
<style>
    table {
        border-collapse:collapse;
    }
    tr,th,td {
        border:solid 1px #000000;
    }
</style>    
</head>
<body>
    <P>以下にファイルから読み込んだ住所データを表示</P>
    <p>住所データ</p>
    <table>
        <tr>
            <th>郵便番号</th>
            <th>都道府県</th>
            <th>市区町村</th>
            <th>町域</th>
        </tr>
<?php foreach($data as $value){ ?>   
<?php //var_dump($value); exit; ?>
        <tr>
            <td><?php print $value[0]; ?></td>
            <td><?php print $value[4]; ?></td>
            <td><?php print $value[5]; ?></td>
            <td><?php print $value[6]; ?></td>
        </tr>
<?php } ?>        
    </table>
</body>
</html>