<?php

$name = '';
$comment = '';
$error = [];
$host = 'localhost';
$username = 'codecamp38342';
$passwd = 'codecamp38342';
$dbname = 'codecamp38342';
$link = mysqli_connect($host, $username, $passwd, $dbname);
$date = date('Y-m-d H:i:s');
$comment_data = [];

        
if ($link) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
           
        if (isset($_POST['name']) === TRUE) {
            $name = $_POST['name'];
        }
        if (isset($_POST['comment']) === TRUE) {
            $comment = $_POST['comment'];
        }
        if (mb_strlen($name) === 0 || $name === '') {
            $error[] = '名前を入力してください'."\n" ;
        }
        if (mb_strlen($name) > 20) {
            $error[] = '名前は20文字以内で書いてください'."\n";
        }
        if (mb_strlen($comment) === 0 || $comment === ' ') {
            $error[] = 'コメントを入力してください'."\n";
        }
        if (mb_strlen($comment) > 100) {
            $error[] = 'コメントは100文字以内で書いてください';
        }
        if (count($error) === 0 && $link) {
            mysqli_set_charset($link, 'utf8');
            $query = "INSERT INTO comment_table(name, message, time) VALUES ('$name', '$comment', '$date')";
            mysqli_query($link, $query);
            // if (mysqli_query($link, $query) === TRUE) {
            //     print '追加成功';
            // }   else {
            //     print '追加失敗';
            //     }
        }
    }
        
    mysqli_set_charset($link, 'utf8');
    $query = 'SELECT name, message, time FROM comment_table';
    $result = mysqli_query($link, $query);
    while ($row = mysqli_fetch_array($result)){
        $comment_data[] = $row;
    }
    mysqli_free_result($result);
    mysqli_close($link);
}   else {
        print 'DB接続失敗';
    }
?>
    



<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>16-11</title>
</head>
<body>
    <h1>ひとこと掲示板</h1>
<?php foreach($error as $message) { ?>   
    <p><?php print $message ?></p>
<?php } ?>    
    <form method="post">
        <label for="name">名前: <input type="text" name="name"></label>
        <label for="comment">コメント: <input type="text" name="comment"></label>
        <input type="submit" value="送信">
    </form>
    <ul>
<?php foreach($comment_data as $value) { ?>  
   <li><?php print htmlspecialchars($value['name'].' '.$value['message'].' '.$value['time'], ENT_QUOTES,'UTF-8'); ?></li>
<?php } ?>
    </ul>
</body>
