<?php
$goods_name = '';
$price = '';
$goods_data = [];
$host = 'localhost';
$username = 'codecamp38342';
$passwd = 'codecamp38342';
$dbname = 'codecamp38342';
$link = mysqli_connect($host, $username, $passwd, $dbname);
mysqli_set_charset($link, 'utf8');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        
    
    if (isset($_POST['goods_name']) === TRUE) {
        $goods_name = $_POST['goods_name'];
    } 
    if (isset($_POST['price']) === TRUE){
        $price = $_POST['price'];
    }    
    
    if ($link) {
        $query = "INSERT INTO goods_table(goods_name, price) VALUES ('$goods_name', $price)";
         if (mysqli_query($link, $query) === TRUE) {
             print '追加成功';
         } else {
           print '追加失敗';
         }
        
    } else {
        print 'DB接続失敗';
    }   
        
}
    
if ($link) {
    $query = 'SELECT goods_name, price FROM goods_table';
    $result = mysqli_query($link, $query);
    while ($row = mysqli_fetch_array($result)){
        $goods_data[] = $row;
    }
    mysqli_free_result($result);
    mysqli_close($link);
}   else {
    print 'DB接続失敗';
}

?>

<!DOCTYPE html>
<html lang="ja">
<heda>
    <meta charset="UTF-8">
    <title>課題２</title>
    <style type="text/css">
        table, td, th {
            border: solid black 1px;
        }
        table {
            width: 200px;
        }
    </style>
</heda>
<body>
    <p>追加したい商品の名前と価格を入力してください</p>
    <form method="POST">
        <p>商品名:<input type="text" name="goods_name" >
        価格:<input type="text" name="price">
        <input type="submit" value="追加"></p>
    </form>
    <table>
        <tr>
            <th>商品名</th>
            <th>価格</th>
        </tr>
<?php
foreach ($goods_data as $value) {
?>
        <tr>
            <td><?php print htmlspecialchars($value['goods_name'],ENT_QUOTES, 'UTF-8'); ?></td>
            <td><?php print htmlspecialchars($value['price'],ENT_QUOTES, 'UTF-8'); ?></td>
        </tr>
<?php
}
?>
    </table>
    
</body>
</html>