<?php
$int1 = 7 + 7;
print $int1;
$int2 = 7 + 7 / 7;
print $int2;
$int3 = 7 + (7 / 7);
print $int3;
$int4 = (7 + 7) / 7;
print $int4;
$int5 = 7 + 7 / 7 + 7;
print $int5;
$int6 = (7 + 7) / 7 + 7;
print $int6;
$int7 = (7 + 7) / (7 + 7);
print $int7;
$int8 = 7 + 7 * 7 / 7 + 7;
print $int8;
$int9 = 7 + (7 + 7 * 7 / 7 + 7);
print $int9;
$hp = 100;
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>演算子の例</title>
</head>
<body>
<h2>初期のHP: <?php print $hp; ?></h2>
<p>攻撃</p>
<?php
$damage = mt_rand(1, 20);
$hp = $hp - $damage;
?>
<p><?php print $damage ?>のダメージ</p>
<P>残りHP: <?php print $hp; ?></P>
<p>追撃</p>
<?php
$damage = mt_rand(1, 20);
$hp = $hp - $damage;
?>
<p><?php print $damage ?>のダメージ</p>
<p>残りHP: <?php print $hp; ?></p>
    
</body>
</html>