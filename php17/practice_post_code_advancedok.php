<?php
$message = [];
$address_data = [];
$host = 'localhost';
$username = 'codecamp38342';
$passwd = 'codecamp38342';
$dbname = 'codecamp38342';
$link = mysqli_connect($host, $username,$passwd, $dbname);
$postal_code = '';
$prefecture = '';
$city = '';

if ($link){
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        
       
        if (isset($_POST['postal_code']) === TRUE) {
            $postal_code = $_POST['postal_code'];
            if (mb_strlen($postal_code) === 0) {
                $message[] = '郵便番号を入力してください';
            } else if (preg_match('/^[0-9]{7}$/',$postal_code) !== 1) {
                $message[] = '7桁の数字を入力してください';
            } 
        }   
       
        if (isset($_POST['prefecture']) === TRUE) {
            $prefecture = $_POST['prefecture'];
            if (mb_strlen($prefecture) === 0) {
                $message[] = '都道府県を選択してください';
            }   
        }
        
        if (isset($_POST['city']) === TRUE) {
            $city = $_POST['city'];
            if (mb_strlen($city) === 0) {
                $message[] = '市区町村を入力してください';
            } else if (preg_match('/^[^0-9]+$/', $city) !== 1) {
                $message[] = '正しく入力してください';
            }
            
        }
        
        if (count($message) ===0) {
            mysqli_set_charset($link, 'utf8');
            $query = " SELECT postal_code, prefecture_kanji, city_kanji, area_kanji FROM address_table" ;
    
            if ($postal_code !== '') {
                $query .= " WHERE postal_code = '{$postal_code}' "; //.= は$query = $query.WHERE~~~の意味//
            } else if($prefecture !=='' && $city !=='') {
                $query.=" WHERE prefecture_kanji='{$prefecture}' AND city_kanji LIKE '%{$city}%'";
            }
            $result = mysqli_query($link, $query);
            var_dump($query);
            while ($row = mysqli_fetch_array($result)) {
                $address_data[] = $row;
            }
            mysqli_free_result($result);
        }
    }
    mysqli_close($link); 
} else {
    print 'DB接続失敗';
}
?>




<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title>郵便番号検索</title>
    <style>
        .search_reslut {
            border-top: solid 1px;
            margin-top: 10px;
        }
        
        table {
            border-collapse: collapse;
        }
        table, tr, th, td {
            border: solid 1px;
        }
        caption {
            text-align: left;
        }
    </style>
</head>
<body>
    <h1>郵便番号検索</h1>
    <?php foreach($message as $value) { ?>
    <p><?php print $value; ?></p>
    <?php } ?>
    <section>
        <h2>郵便番号から検索</h2>
        <form method="POST">
            <input type="text" name="postal_code" placeholder="例）1010001" value="">
            <!--<input type="hidden" name="search_method" value="zipcode">-->
            <input type="submit" value="検索">
        </form>
        <h2>地名から検索</h2>
        <form method="POST">
            都道府県を選択
            <select name="prefecture">
                <option value="" selected>都道府県を選択</option>
                <option value="北海道" >北海道</option>
                <option value="青森県" >青森県</option>
                <option value="岩手県" >岩手県</option>
                <option value="宮城県" >宮城県</option>
                <option value="秋田県" >秋田県</option>
                <option value="山形県" >山形県</option>
                <option value="福島県" >福島県</option>
                <option value="茨城県" >茨城県</option>
                <option value="栃木県" >栃木県</option>
                <option value="群馬県" >群馬県</option>
                <option value="埼玉県" >埼玉県</option>
                <option value="千葉県" >千葉県</option>
                <option value="東京都" >東京都</option>
                <option value="神奈川県" >神奈川県</option>
                <option value="新潟県" >新潟県</option>
                <option value="富山県" >富山県</option>
                <option value="石川県" >石川県</option>
                <option value="福井県" >福井県</option>
                <option value="山梨県" >山梨県</option>
                <option value="長野県" >長野県</option>
                <option value="岐阜県" >岐阜県</option>
                <option value="静岡県" >静岡県</option>
                <option value="愛知県" >愛知県</option>
                <option value="三重県" >三重県</option>
                <option value="滋賀県" >滋賀県</option>
                <option value="京都府" >京都府</option>
                <option value="大阪府" >大阪府</option>
                <option value="兵庫県" >兵庫県</option>
                <option value="奈良県" >奈良県</option>
                <option value="和歌山県" >和歌山県</option>
                <option value="鳥取県" >鳥取県</option>
                <option value="島根県" >島根県</option>
                <option value="岡山県" >岡山県</option>
                <option value="広島県" >広島県</option>
                <option value="山口県" >山口県</option>
                <option value="徳島県" >徳島県</option>
                <option value="香川県" >香川県</option>
                <option value="愛媛県" >愛媛県</option>
                <option value="高知県" >高知県</option>
                <option value="福岡県" >福岡県</option>
                <option value="佐賀県" >佐賀県</option>
                <option value="長崎県" >長崎県</option>
                <option value="熊本県" >熊本県</option>
                <option value="大分県" >大分県</option>
                <option value="宮崎県" >宮崎県</option>
                <option value="鹿児島県" >鹿児島県</option>
                <option value="沖縄県" >沖縄県</option>
            </select>
            市区町村
            <input type="text" name="city" value="">
            <!--<input type="hidden" name="search_method" value="address">-->
            <input type="submit" value="検索">
        </form>
    </section>
    <section class="search_reslut">
        <p>ここに検索結果が表示されます</p>
<?php if ($_SERVER['REQUEST_METHOD'] === 'POST') { ?>
        <table>
            <tr>
                <th>郵便番号</th>
                <th>都道府県</th>
                <th>市区町村</th>
                <th>町域</th>
            </tr>    

<?php
foreach ($address_data as $value) {
?>
            <tr>
                <td><?php print htmlspecialchars($value['postal_code'], ENT_QUOTES, 'UTF-8'); ?></td>
                <td><?php print htmlspecialchars($value['prefecture_kanji'], ENT_QUOTES, 'UTF-8'); ?></td>
                <td><?php print htmlspecialchars($value['city_kanji'], ENT_QUOTES, 'UTF-8'); ?></td>
                <td><?php print htmlspecialchars($value['area_kanji'], ENT_QUOTES, 'UTF-8'); ?></td>
            </tr>
<?php
}
?>
       
        </table>
<?php
}
?>
    </section>
</body>
</html>