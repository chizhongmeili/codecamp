<?php
$message = [];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
   
    if (isset($_POST['mail_address']) === TRUE) {
        $mail_address = $_POST['mail_address'];
    }
    if (isset($_POST['password']) === TRUE) {
        $password = $_POST['password'];
    }
    
    if (mb_strlen($mail_address) === 0 ) {
        $message[] = 'アドレスを入力してください';
    } else if (preg_match('/^\S+@\S+$/', $mail_address) !== 1) {
        $message[] = 'アドレスの形式が正しくありません';
    }  
   
    if (mb_strlen($password) === 0 ) {
        $message[] = 'パスワードを入力してください';
    } else if (preg_match('/^[!-~]{6,18}$/', $password) !== 1) {
        $message[] = 'パスワードの形式が正しくありません';
    }
    //アドレスもパスワードも成功だった時に登録成功と
    //コメントを出すには？
}
?>
<!DOCTYPE HTML>
<html lang="ja">
<head>
   <meta charset="UTF-8">
   <title>17-5</title>
</head>

<body>
<?php if (count($message) === 0 && $_SERVER['REQUEST_METHOD'] === 'POST') { ?>
<p>登録完了</p>
<?php } else { ?>
    <form method="POST">
        <label for="mail_address">メールアドレス</label>
        <input id="mail_address" type="text" name="mail_address" value="<?php if (isset($mail_address) === TRUE) { print $mail_address; }?>">;
        <label for="password">パスワード</label>
        <input id="password" type="password" name="password" value="<?php if (isset($password) === TRUE) { print $password; }?>">;
        <input type="submit" value="Submit"/>
    </form>
<?php foreach($message as $msg) { ?>
<p><?php print $msg; ?></p>
<?php } ?>
<?php } ?>
</body>
</html>


<!--パスワードは半角英数字記号で6文字以上18文字以下のみ可能とします。-->
<!--また「未入力」と「入力はあるが値に問題がある」はそれぞれ異なる-->
<!--エラーメッセージを用意し表示してください。-->