<?php
$my_name = '';
if (isset($_POST['my_name']) === TRUE ) {
    $my_name = htmlspecialchars($_POST['my_name'],ENT_QUOTES,'UTF-8') ;
}   
$gender = '';
if (isset($_POST['gender']) === TRUE ) {
    $gender = htmlspecialchars($_POST['gender'],ENT_QUOTES,'UTF-8') ;
}

$mail = '';
if (isset($_POST['mail']) === TRUE ) {
    $mail = htmlspecialchars($_POST['mail'],ENT_QUOTES,'UTF-8'). "\n";
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>7-7</title>
</head>
<body>
<h1>課題</h1> 
<?php if ($my_name !== ''){ ?>
    <p>ここに選択した名前を表示<?php print $my_name; ?></p>
<?php } ?>

<?php if ($gender !== ''){ ?>
    <p>ここに選択した性別を表示<?php print $gender; ?></p>
<?php } ?>

<?php if ($mail !== ''){ ?>
    <p>ここにメールを受け取るかを表示<?php print $mail; ?></p>
<?php } ?>


<form method="post">
    <p>
        <label for="my_name">お名前: </label>
        <input id="my_name" type="text" name="my_name" value="<?php print $my_name; ?>">
    </p>
   
    <p>性別:
        <input type="radio" name="gender" value="man">男
        <input type="radio" name="gender" value="woman">女
    </p>
    <p>
        <input type="checkbox" name="mail" value="OK">お知らせメールを受け取る
    </p>
    <p>
    <input type="submit" value="送信">
    </p>
</form>
</body>
</html>