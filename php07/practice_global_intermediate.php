<?php
$hand = ['グー','チョキ','パー'];
$key = array_rand($hand);
$pc_hand = $hand[$key];

$player = '';
if (isset($_POST['player']) === TRUE) {
    $player = htmlspecialchars($_POST['player'],ENT_QUOTES,'UTF-8');

    if ($player === $pc_hand){
        $result = 'drow';
    }else if ($player === 'グー' && $pc_hand === 'チョキ'){
        $result = 'Win';
    }else if ($player === 'チョキ' && $pc_hand === 'パー'){
        $result = 'Win';
    }else if ($player === 'パー' && $pc_hand === 'グー'){
        $result = 'Win';
    }else {
        $result = 'loose';
    }
    
}


?>
<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>じゃんけん勝負</title>
</head>
<body>
<h1>じゃんけん勝負</h1>
<?php if($player !== ''){ ?>
    <p>自分: <?php print $player ; ?></p>
    <p>PC: <?php print $pc_hand ; ?></p>
    <p>結果: <?php print $result ; ?></p>
    
<?php } ?>
<form method="post">
    <input type="radio" name="player" value="グー">グー
    <input type="radio" name="player" value="チョキ">チョキ
    <input type="radio" name="player" value="パー">パー
    <p><input type="submit" value="勝負!"/></p>
</form>

</body>
</html>