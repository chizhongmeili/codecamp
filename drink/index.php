
<?php
$host = 'localhost';
$username = 'codecamp38342';
$passwd = 'codecamp38342';
$dbname = 'codecamp38342';
$folder = './img_file/';
$link = mysqli_connect($host, $username, $passwd, $dbname);
$drink_data = [];
if ($link) {
    mysqli_set_charset($link,'utf8');
    $query = 'SELECT drink_table.drink_id, drink_table.pic, drink_table.drink_name, drink_table.price, drink_table.status, drink_stock_table.stock';
    $query.= ' FROM drink_table JOIN drink_stock_table ON drink_table.drink_id = drink_stock_table.drink_id';
    $result = mysqli_query($link,$query);
    while ($row = mysqli_fetch_array($result)) {
        $drink_data[] = $row;
    }
    mysqli_free_result($result);
    mysqli_close($link);
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title>自動販売機</title>
    <style>
        #flex {
            width: 600px;
        }

        #flex .drink {
            //border: solid 1px;
            width: 120px;
            height: 210px;
            text-align: center;
            margin: 10px;
            float: left; 
        }

        #flex span {
            display: block;
            margin: 3px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .img_size {
            height: 125px;
        }

        .red {
            color: #FF0000;
        }

        #submit {
            clear: both;
        }

    </style>
</head>
<body>
    
    <h1>自動販売機</h1>
    <form action="result.php" method="post">
        <div>金額<input type="text" name="money" value=""></div>
        <div id="flex">
            
            <?php foreach($drink_data as $value) { ?>   
            <?php if ($value['status'] === '2') { ?>
                <div class="drink">
                        <span class="img_size"><img src="<?php print $folder.htmlspecialchars($value['pic'],ENT_QUOTES,'UTF-8'); ?>"></span>
                        <span><?php print htmlspecialchars($value['drink_name'],ENT_QUOTES,'UTF-8'); ?></span>
                        <span><?php print htmlspecialchars($value['price'],ENT_QUOTES,'UTF-8'); ?></span>
                        <?php if ($value['stock'] > 0) { ?>
                            <input type="radio" name="drink_id" value="<?php print htmlspecialchars($value['drink_id'],ENT_QUOTES,'UTF-8'); ?>">
                            <?php } else { ?>
                                <span class="red">売り切れ</span>
                            <?php } ?>
                </div>
            <?php } ?>
            <?php } ?>
        </div>
        <div id="submit">
            <input type="submit" value="■□■□■ 購入 ■□■□■">
        </div>
    </form>
</body>
</html>