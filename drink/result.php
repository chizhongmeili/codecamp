<?php 
$host = 'localhost';
$username = 'codecamp38342';
$passwd = 'codecamp38342';
$dbname = 'codecamp38342';
$result_data = [];
$err_msg = [];
$folder = './img_file/';
$link = mysqli_connect($host, $username, $passwd, $dbname);

if ($link) {
    mysqli_set_charset($link,'utf8');
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        
        $money = '';
        if(isset($_POST['money']) === TRUE) {
            $money = $_POST['money'];
        }
        if ($money === '') {
            $err_msg[] = '金額を投入してください';
        } else if (preg_match('/^[0-9]+$/',$money) !== 1) {
            $err_msg[] = '金額は0以上の整数を入力してください';
        }
        $drink_id = '';
        if(isset($_POST['drink_id']) === TRUE) {
            $drink_id = $_POST['drink_id'];
        }
        if ($drink_id === '') {
            $err_msg[] = '商品が選択されていません';
        }
        if (count($err_msg) === 0) {
            $query = 'SELECT status, pic, price, drink_name, stock 
                    FROM drink_table JOIN drink_stock_table ON drink_table.drink_id = drink_stock_table.drink_id
                    WHERE drink_table.drink_id = '.$drink_id;
            if ($result = mysqli_query($link, $query)) {
                while ($row = mysqli_fetch_array($result)) {
                    $result_data[] = $row;
                }
                mysqli_free_result($result);
                if (count($result_data) > 0) {
                    //$dorink = $result_data[0];
                    //$dorink['stock']
                    foreach($result_data as $value) {
                        if ($value['stock'] <= 0) {
                            $err_msg[] = '商品の在庫がありません';
                        }
                        if ( (int)$value['status'] !== 2) {
                            $err_msg[] = 'ステータスが非公開になってます';
                        }
                        if ($value['price'] > $money) {
                            $err_msg[] = '投入金額が不足しています';
                        } 
                    }
                } else {
                    $err_msg[] = '商品情報の取得失敗';
                }
            } else {
                $err_msg[] = '商品情報の取得失敗';
            }
        }
       
        
        if (count($err_msg) === 0) {
            
           mysqli_autocommit($link,false);
           $query = 'UPDATE drink_stock_table SET stock = stock-1, update_date = now() WHERE drink_id = '.$drink_id;
           if (mysqli_query($link,$query) === TRUE) {
               $query = "INSERT INTO purchase_history_table(drink_id,purchase_date) VALUES({$drink_id},now())";
               if (mysqli_query($link, $query) !== TRUE) {
                   $err_msg[] = '商品情報の登録失敗';
               }
            } else {
                 $err_msg[] = '商品情報の登録失敗';
            }
            
            if (count($err_msg) === 0) {
                mysqli_commit($link);
            } else {
                mysqli_rollback($link) ;
            }
       }
            
   } mysqli_close($link);

} else {
    print 'DB接続エラー';
}
        
   
   
   
   
   

?> 
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title>自動販売機結果</title>
</head>
<body>
    <h1>自動販売機結果</h1>
    <?php if (count($err_msg) !== 0) { ?>
        <?php foreach($err_msg as $value) { ?>   
            <p><?php print $value; ?></p>
        <?php } ?>
        <footer><a href="index.php">戻る</a></footer>
    <?php } else { ?>
       <?php foreach($result_data as $value) { ?>
        <img src="<?php print $folder.htmlspecialchars($value['pic'],ENT_QUOTES,'UTF-8') ;?>">
        <p>がしゃん！【<?php print htmlspecialchars($value['drink_name'],ENT_QUOTES,'UTF-8') ;?>】が買えました！</p>
        <p>おつりは【<?php print htmlspecialchars($money - $value['price'],ENT_QUOTES,'UTF-8') ;?>】です</p>
       <?php } ?>
     <footer><a href="index.php">戻る</a></footer>
   <?php } ?>
</body>
</html>