<?php
$user = [];
$succ_msg = '';
$new_name = '';
$new_passwd = '';
$err_msg = [];
$host = 'localhost';
$username = 'root';
$passwd = '';
$dbname = 'codecamp';
$link = mysqli_connect($host,$username,$passwd,$dbname);
if ($link) {
    mysqli_set_charset($link,'utf8');
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['new_name']) === TRUE) {
            $new_name = $_POST['new_name'];
        }
        if (isset($_POST['new_passwd']) === TRUE) {
            $new_passwd = $_POST['new_passwd'];
        } 
        if (isset($_POST['user_id']) === TRUE) {
            $user_id = $_POST['user_id'];
        }
        $query = 'SELECT user_name FROM shop_user_table WHERE user_name =\''.$new_name.'\'';
        if ($result = mysqli_query($link,$query)) {
            $row = mysqli_fetch_assoc($result);
            if ($row['user_name'] === $new_name) {
                $err_msg[] = '既存のユーザーネームです。違う名前を登録してください';
            }
        } 
        
        if ($new_name === '' ) {
                $err_msg[] = 'ユーザー名を入力してください';
            } else if (!preg_match('/^[a-zA-Z0-9]{6,}$/',$new_name)) {
                $err_msg[] = 'ユーザー名は半角英数字、6文字以上で入力してください';
            }
        if ($new_passwd === '' ) {
            $err_msg[] = 'パスワードを入力してください';
        } else if (!preg_match('/^[a-zA-Z0-9]{6,}$/',$new_passwd)) {
            $err_msg[] = 'パスワードは半角英数字、6文字以上で入力してください';
        }
        if (count($err_msg) === 0) {
            $query = "INSERT INTO shop_user_table(user_name,password,created_date,update_date) VALUES('".$new_name."','".$new_passwd."',now(),now())";
            mysqli_query($link,$query);    
            $user_id = mysqli_insert_id($link);
            $succ_msg = 'ユーザー登録完了';
        } else {
            $err_msg[] = 'ユーザー情報入力失敗';
        }        
    } 
} else {
    $err_msg[] = 'DBに接続失敗';
}
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title>ユーザー登録</title>
    <style>
    div {
        padding-top: 20px;
        padding-left: 20px;
    }
    </style>
</head>
<body>
<?php if(count($err_msg) !== 0) {
        foreach($err_msg as $value) { ?>
    <p><?php print $value ;?></p>
<?php } ?>
<?php } ?>
<?php if(!empty($succ_msg)) { ?>
    <p><?php print $succ_msg; ?></p>
<?php } ?> 
    <a href="login.php">ログインページ</a>
    <h1>ユーザー登録</h1>
    <form method="post">
        <div><label>ユーザー名: <input type="text" name="new_name" value=""></label></div>
        <div><label>パスワード: <input type="password" name="new_passwd" value=""></label></div>
        <input type="hidden" name="user_id" value="">
        <div><input type="submit" value="ユーザー登録"></div>
    </form>
</body>