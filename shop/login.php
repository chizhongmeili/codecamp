<?php
$host = 'localhost';
$username = 'root';
$passwd = '';
$dbname = 'codecamp';
$link = mysqli_connect($host,$username,$passwd,$dbname);
$username = '';
$passwd = '';
$err_msg = [];

session_start();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['user_name']) === TRUE) {
        $username = $_POST['user_name'];
    }
    if (isset($_POST['password']) === TRUE) {
        $passwd = $_POST['password'];
    }
    $sql = "SELECT * FROM shop_user_table WHERE user_name = '{$username}' AND password = '{$passwd}'";
    if ($result = mysqli_query($link,$sql)) {
        if ($user = mysqli_fetch_assoc($result)) {
            $_SESSION['user_id'] = $user['user_id'];
            $_SESSION['user_name'] = $user['user_name'];
        } else {
            $err_msg[] = 'ユーザー名もしくはパスワードが違います';
        }
    } else {
        $err_msg[] = 'ユーザー情報の取得に失敗';
    }
}


if (isset($_GET['action_kind']) === TRUE) {
    if($_GET['action_kind'] === 'logout') {
        
        $_SESSION = [];
        session_destroy();
    }
}
 
if (isset($_SESSION['user_name']) === TRUE && count($err_msg) === 0) {
    if ($_SESSION['user_name'] === 'admin') {
        header('location:tool.php');
    } else {
        header('location:index.php');
    }
    exit;
} 

?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title>ログインページ</title>
    <link type="text/css" rel="stylesheet" href="./css/common.css">
    <style>
        .system_title {
          color: white;
          font-size:30px;
        }
    </style>
</head>
<body>
<?php if (count($err_msg) !== 0) {
        foreach($err_msg as $value) { ?>
        <p><?php print $value ?></p>
<?php } ?> 
<?php } ?> 

<header>
    <div class="header-box">
        <span class="system_title">mamazon</span>
    </div>
</header>

<div class="content">
    <div class="login">
        <form method="post" action="./login.php">
            <div><input type="text" name="user_name" placeholder="ユーザー名"></div>
            <div><input type="password" name="password" placeholder="パスワード">
            <div><input type="submit" value="ログイン">
        </form>
    
        <div class="account-create">
            <a href="./register.php">ユーザーの新規作成</a>
        </div>
    </div>
</div>

</body>
</html>
