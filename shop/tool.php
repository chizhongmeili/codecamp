<?php
$new_name = '';
$new_price = '';
$new_stock = '';
$new_img = '';
$new_status = '';
$host = 'localhost';
$username = 'root';
$passwd = '';
$dbname = 'codecamp';
$err_msg = [];
$folder = './img_file/';
$succ_msg = '';


session_start();
if (isset($_SESSION['user_id']) === false) {
    header('location:login.php');
    exit;
}


function close_db_connect($link){
    mysqli_close($link);
}


$link = mysqli_connect($host, $username, $passwd, $dbname);

if ($link) {
    mysqli_set_charset($link, 'utf8');

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['sql_kind']) === TRUE) {
            $sql_kind = $_POST['sql_kind'];
        }
        $date = date('Y-m-d H:i:s');

        if ($sql_kind === 'insert') {

            if (isset($_POST['new_name']) === TRUE) {
                $new_name = $_POST['new_name'];
            }
            if (isset($_POST['new_price']) === TRUE) {
                $new_price = $_POST['new_price'];
            }
            if (isset($_POST['new_stock']) === TRUE) {
                $new_stock = $_POST['new_stock'];
            }
            
            if (isset($_POST['new_status']) === TRUE) {
                $new_status = $_POST['new_status'];
            }
            
            if ($new_name === '') {
                $err_msg[] = '商品名を入力してください';
            } else if (preg_match('/( |　)+/', $new_name)) {
                $err_msg[] = '空白スペースは入力しないでください';
            }
            
            
            if ($new_price === '') {
                $err_msg[] = '値段を入力してください';
            } else if (preg_match('/( |　)+/', $new_price)) {
                $err_msg[] = '空白スペースは入力しないでください';
            } else if (preg_match('/^[0-9]+$/', $new_price) !== 1) {
                $err_msg[] = '値段は0以上の整数値を入力してください';
            }
            
            
            if ($new_stock === '') {
                $err_msg[] = '個数を入力してください';
            } else if (preg_match('/( |　)+/', $new_stock)) {
                $err_msg[] = '空白スペースは入力しないでください';
            } else if (preg_match('/^[0-9]+$/', $new_stock) !== 1) {
                $err_msg[] = '個数は0以上の整数値を入力してください';
            }
            
            if ($new_status !== '1' && $new_status !== '2') {
                $err_msg[] = 'ステータスを選択してください';
            }

            if (is_uploaded_file($_FILES['new_img']['tmp_name']) === TRUE) {
                $type = exif_imagetype($_FILES['new_img']['tmp_name']);
                if ($type === false) {
                    $err_msg[] = '画像ファイルを選択してください';
                } else if ($type !== IMAGETYPE_JPEG && $type !== IMAGETYPE_PNG) {
                    $err_msg[] = '画像ファイルの形式が違います';
                } else {
                    $new_img = $_FILES['new_img']['name'];
                    if (move_uploaded_file($_FILES['new_img']['tmp_name'], $folder . $new_img) === false) {
                        $err_msg[] = '画像ファイルのアップロード失敗';
                    }
                }
            } else {
            $err_msg[] = '画像ファイルを選択してください';
            }

            if (count($err_msg) === 0) {
                mysqli_autocommit($link, false);
            
                $data = [
                'name' => $new_name,
                'price' => $new_price,
                'created_date' => $date,
                'update_date' => $date,
                'status' => $new_status,
                'img' => $new_img
                ];
                $query = 'INSERT INTO shop_item_table(name, price, created_date, update_date, status, img)';
                $query .= ' VALUES (\'' . implode('\',\'', $data) . '\')';
                
                if (mysqli_query($link, $query) === TRUE) {
                    $item_id = mysqli_insert_id($link);
            
                    $data = [
                    'item_id' => $item_id,
                    'stock' => $new_stock,
                    'created_date' => $date,
                    'update_date' => $date
                    ];
            
                    $query = 'INSERT INTO shop_stock_table(item_id, stock, created_date, update_date)';
                    $query .= ' VALUES (\'' . implode('\',\'', $data) . '\')';
                    if (mysqli_query($link, $query) === false) {
                        $err_msg[] = "SQL実行失敗:" . $query;
                    }
                    } else {
                        $err_msg[] = "SQL実行失敗:" . $query;
                    }
            
                
                
                if (count($err_msg) === 0) {
                    mysqli_commit($link);
                    $succ_msg = '商品の追加が完了しました';
                } else {
                    mysqli_rollback($link);
                }
            }
        } else if ($sql_kind === 'update') {
                
            $update = date('Y-m-d H:i:s');
            
            if (isset($_POST['update_stock']) === TRUE) {
                $update_stock = $_POST['update_stock'];
            }
            if (isset($_POST['item_id']) === TRUE) {
                $item_id = $_POST['item_id'];
            }
            
            
            if (preg_match('/( |　)+/', $update_stock)) {
                $err_msg[] = '空白スペースは入力しないでください';
            } else if (preg_match('/^[0-9]+$/', $update_stock) !== 1) {
                $err_msg[] = '個数は0以上の整数を入力してください';
            }
            
            if (count($err_msg) === 0) {
               
                $query = "UPDATE shop_stock_table SET stock = '" . $update_stock . "',update_date = '" . $update . "' WHERE item_id = '" . $item_id . "'";
                if (mysqli_query($link, $query) === TRUE) {
                    $succ_msg = '在庫数が正常に完了しました';
                } else {
                    $err_msg[] = '在庫変更に失敗しました';
                }
            }
        
        } else if ($sql_kind === 'change') {
            if (isset($_POST['change_status']) === TRUE) {
                $change_status = $_POST['change_status'];
            }
            if (isset($_POST['item_id']) === TRUE) {
                $item_id = $_POST['item_id'];
            }

            if (preg_match('/^[12]$/', $change_status) === 1) {
                $query = "UPDATE shop_item_table SET status = '" . $change_status . "' WHERE item_id = '" . $item_id . "'";
                if (mysqli_query($link, $query) === TRUE) {
                    $succ_msg = 'ステータスの変更が正常に完了しました';
                } else {
                    $err_msg[] ='ステータスの変更に失敗しました';
                }
            } else {
                $err_msg[] = 'ステータスのvalueは1か2を選択してください';
            }
        
            
        } else if ($sql_kind === 'delete') {
            if (isset($_POST['item_id']) === TRUE) {
                $item_id = $_POST['item_id'];
            }
            
            mysqli_autocommit($link,false);
            
            $query = 'DELETE FROM shop_item_table WHERE item_id =' . $item_id;
            if (mysqli_query($link, $query)) {
                $query = 'DELETE FROM shop_stock_table WHERE item_id =' . $item_id;
                if (mysqli_query($link, $query) === false) {
                    $err_msg[] = '在庫テーブルの削除に失敗しました';
                }
            }else {
                $err_msg[] = '商品テーブルの削除に失敗しました';
            } 
            if (count($err_msg) === 0) {
                mysqli_commit($link);
                $succ_msg = 'テーブルの削除が正常に完了しました';
            } else {
                mysqli_rollback($link);
            }
        }
    }


    $query = 'SELECT shop_item_table.item_id, img, name, price, stock, status';
    $query .= ' FROM shop_item_table JOIN shop_stock_table ON shop_item_table.item_id = shop_stock_table.item_id';
    $item_data = [];
    if ($result = mysqli_query($link, $query)) {
        while ($row = mysqli_fetch_array($result)) {
        $item_data[] = $row;
        }
        mysqli_free_result($result);
        close_db_connect($link);
    } else {
        print '商品の情報取得失敗';
    }
}

?>


<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title>商品管理ページ</title>
    <style>
        section {
        margin-bottom: 20px;
        border-top: solid 1px;
        }
    
        table {
        width: 660px;
        border-collapse: collapse;
        }
    
        table,
        tr,
        th,
        td {
        border: solid 1px;
        padding: 10px;
        text-align: center;
        }
    
        caption {
        text-align: left;
        }
    
        .text_align_right {
        text-align: right;
        }
    
        .drink_name_width {
        width: 100px;
        }
    
        .input_text_width {
        width: 60px;
        }
    
        .status_false {
        background-color: #A9A9A9;
        }
    </style>
</head>

<body>
    <a href="user.php">ユーザー管理ページ</a>
    <a href="login.php?action_kind=logout">ログアウト</a>

<?php if (count($err_msg) !== 0) {
    foreach ($err_msg as $value) { ?>
    <p><?php print $value; ?></p>
    <?php } ?>
<?php } ?>

<?php if (empty($succ_msg) === false) { ?>
    <p><?php print $succ_msg; ?></p>
<?php } ?>
    
    <h1>mamazon商品管理ページ</h1>
<section>
    <h2>新規商品追加</h2>
    <form method="post" enctype="multipart/form-data">
        <div><label>名前: <input type="text" name="new_name" value="<?php print $new_name; ?>"></label></div>
        <div><label>値段: <input type="text" name="new_price" value="<?php print $new_price; ?>"></label></div>
        <div><label>在庫数: <input type="text" name="new_stock" value="<?php print $new_stock; ?>"></label></div>
        <div><input type="file" name="new_img"></div>
        <div>
            <select name="new_status">
            <option value="1">非公開</option>
            <option value="2">公開</option>
            </select>
        </div>
        <input type="hidden" name="sql_kind" value="insert">
        <div><input type="submit" value="■□■□■商品追加■□■□■"></div>
    </form>
</section>

<section>
    <h2>商品情報変更</h2>
    <table>
        <caption>商品一覧</caption>
        <tr>
            <th>商品画像</th>
            <th>商品名</th>
            <th>価格</th>
            <th>在庫数</th>
            <th>ステータス</th>
            <th>操作</th>
        </tr>
        
        <?php foreach ($item_data as $value) { ?>
        <tr <?php if ($value['status'] === '1') { ?> class="status_false" <?php } ?>>
            <form method="post">
                <td><img src="<?php print $folder . htmlspecialchars($value['img'], ENT_QUOTES, 'UTF-8'); ?>"></td>
                <td class="drink_name_width"><?php print htmlspecialchars($value['name'], ENT_QUOTES, 'UTF-8'); ?></td>
                <td class="text_align_right"><?php print htmlspecialchars($value['price'], ENT_QUOTES, 'UTF-8'); ?></td>
                <td><input type="text" class="input_text_width text_align_right" name="update_stock" value="<?php print htmlspecialchars($value['stock'], ENT_QUOTES, 'UTF-8'); ?>">個&nbsp;&nbsp;<input type="submit" value="変更"></td>
                <input type="hidden" name="item_id" value="<?php print htmlspecialchars($value['item_id'], ENT_QUOTES, 'UTF-8'); ?>">
                <input type="hidden" name="sql_kind" value="update">
            </form>
            
            <form method="post">
                <td>
                    <?php if ($value['status'] === '1') { ?>
                    <input type="submit" value="非公開 → 公開">
                    <input type="hidden" name="change_status" value="2">
                    <?php } else if ($value['status'] === '2') { ?>
                    <input type="submit" value="公開 → 非公開">
                    <input type="hidden" name="change_status" value="1">
                    <?php } ?>
                    <input type="hidden" name="item_id" value="<?php print $value['item_id']; ?>">
                    <input type="hidden" name="sql_kind" value="change">
                </td>
            </form>
            
            <form method="post">
                <td>
                    <input type="hidden" name="sql_kind" value="delete">
                    <input type="hidden" name="item_id" value="<?php print $value['item_id']; ?>">
                    <input type="submit" value="削除">
                </td>
            </form>
        </tr>
        <?php } ?>
    </table>
</section>

</body>

</html>