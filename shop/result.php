<?php
$host = 'localhost';
$username = 'root';
$passwd = '';
$dbname = 'codecamp';
$link = mysqli_connect($host,$username,$passwd,$dbname);
$result_data = [];
$folder = './img_file/';
$total_price = '0';
$err_msg = [];

session_start();
if (isset($_SESSION['user_id']) === false) {
    header('location:login.php');
    exit;
} else {
    $user_id = $_SESSION['user_id'];
}
if ($link) {
  mysqli_set_charset($link,'utf8');
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $query = "SELECT cart_table.item_id,amount,price,img,name FROM cart_table 
              INNER JOIN shop_item_table ON cart_table.item_id = shop_item_table.item_id 
              WHERE cart_table.user_id =".$user_id;
    $result = mysqli_query($link,$query);
    while($row = mysqli_fetch_array($result)) {
      $result_data[] = $row;
    }    
    $query = 'SELECT SUM(cart_table.amount * shop_item_table.price) total_price
              FROM cart_table INNER JOIN shop_item_table
              ON cart_table.item_id = shop_item_table.item_id 
              WHERE cart_table.user_id ='.$user_id;
    $result = mysqli_query($link,$query);
    $row = mysqli_fetch_array($result);
    $total_price = $row[0];
    mysqli_free_result($result);
    
   
   
    mysqli_autocommit($link,false);
    
    foreach($result_data as $value) {
      $query = "UPDATE shop_stock_table SET stock = stock - ".$value['amount'].", update_date = now()
                WHERE item_id = ".$value['item_id'];
      
      if (mysqli_query($link,$query) === false) {
         $err_msg[] = '更新処理に失敗しました';
         break;
      }
    } 
    if (count($err_msg) === 0) {
      $query = 'DELETE FROM cart_table WHERE user_id = '.$user_id;
      if (mysqli_query($link,$query) === false) {
        $err_msg[] = '削除処理に失敗しました';
      }
    }
    if (count($err_msg) === 0) {
         // 処理確定
         mysqli_commit($link);
    } else {
         // 処理取消
         mysqli_rollback($link);
    }
    
    mysqli_close($link);
  }  

} else {
  $err_msg[] = 'DB接続失敗';
}


?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>購入完了ページ</title>
  <link type="text/css" rel="stylesheet" href="./css/common.css">
<style>
    .system_title {
      color: white;
      font-size:30px;
      
    }
  </style>
</head>
<body>
  <header>
    <div class="header-box">
      <a href="./index.php">
        <span class="system_title">mamazon</span>
      </a>
      <a class="nemu" href="./login.php?action_kind=logout">ログアウト</a>
      <a href="./index.php" class="nemu">商品一覧ページへ戻る</a>
    </div>
  </header>
  <div class="content">
<?php foreach($err_msg as $value) { ?>  
  <p class="err-msg"><?php print $value; ?></p>
<?php } ?>
    <div class="finish-msg">ご購入ありがとうございました。</div>
    <div class="cart-list-title">
      <span class="cart-list-price">価格</span>
      <span class="cart-list-num">数量</span>
    </div>
      <ul class="cart-list">
<?php foreach($result_data as $value) { ?>    
        <li>
          <div class="cart-item">
            <img class="cart-item-img" src="<?php print $folder.htmlspecialchars($value['img'],ENT_QUOTES,'UTF-8'); ?>">
            <span class="cart-item-name"><?php print htmlspecialchars($value['name'],ENT_QUOTES,'UTF-8'); ?></span>
            <span class="cart-item-price">¥<?php print htmlspecialchars($value['price'],ENT_QUOTES,'UTF-8'); ?></span>
            <span class="finish-item-price"><?php print htmlspecialchars($value['amount'],ENT_QUOTES,'UTF-8'); ?></span>
          </div>
        </li>
<?php } ?>     
      </ul>
    <div class="buy-sum-box">
      <span class="buy-sum-title">合計</span>
      <span class="buy-sum-price">¥<?php print $total_price; ?></span>
    </div>
  </div>
</body>
</html>
