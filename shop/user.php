<?php
$msg = '';
$user_data = [];
$host = 'localhost';
$username = 'root';
$passwd = '';
$dbname = 'codecamp';
session_start();
if (isset($_SESSION['user_id']) === false) {
    header('location:login.php');
    exit;
}
$link = mysqli_connect($host,$username,$passwd,$dbname);
if ($link) {
    mysqli_set_charset($link,'utf8');
    $query = 'SELECT user_name,created_date FROM shop_user_table';
    if ($result = mysqli_query($link,$query)) {
        while ($row = mysqli_fetch_array($result)) {
            $user_data[] = $row;
        }
        mysqli_free_result($result);
        mysqli_close($link);
    }
} $msg = 'DBに接続失敗';
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title>ユーザー管理ページ</title>
    <style>
     table {
            border-collapse: collapse;
        }
        
        table,tr,th,td {
            border: solid 1px;
            padding: 10px;
            text-align: center;
        }
    </style>
</head>
<body>
    <a href="tool.php">商品管理ページ</a>
    <a href="login.php?action_kind='login'">ログアウト</a>
    <h1>mamazonユーザー情報一覧</h1>
    <table>
        <tr>
            <th>ユーザーID</th>
            <th>登録日</th>
        </tr>
<?php foreach($user_data as $value) { ?>        
        <tr>
            <td><?php print htmlspecialchars($value['user_name'],ENT_QUOTES,'UTF-8'); ?></td>
            <td><?php print $value['created_date']; ?></td>
        </tr>
<?php } ?>    
    </table>
</body>
</html>