<?php
$host = 'localhost';
$username = 'root';
$passwd = '';
$dbname = 'codecamp';
$link = mysqli_connect($host,$username,$passwd,$dbname);
$item_data = [];
$folder = './img_file/';
$err_msg = [];
session_start();
if (isset($_SESSION['user_id']) === false) {
    header('location:login.php');
    exit;
} else {
    $user_id = $_SESSION['user_id'];
}

if ($link) {
    mysqli_set_charset($link,'utf8');
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['sql_kind']) ===TRUE) {
            $sql_kind = $_POST['sql_kind'];
        }
        if ($sql_kind === 'insert_cart') {
            
            if (isset($_POST['item_id']) === TRUE) {
                $item_id = $_POST['item_id'];
            }
            
            $query = 'SELECT COUNT(*) count FROM cart_table WHERE user_id =\''.$user_id.'\' AND item_id =\''.$item_id.'\'';
            if ($result = mysqli_query($link,$query)) {
                $row = mysqli_fetch_assoc($result);
                $count = $row['count'];
                if ($count === '0') {
                   
                    $query = 'INSERT INTO cart_table(user_id,item_id,amount,created_date,updated_date)';
                    $query .= " VALUES('".$user_id."','".$item_id."',1,now(),now())";
                } else {
                    $query = 'UPDATE cart_table SET amount = amount+1 , updated_date = now() WHERE user_id =\''.$user_id.'\' AND item_id =\''.$item_id.'\'';
                }
                if (mysqli_query($link,$query) !== true) {
                    $err_msg[] = 'カートへの追加に失敗しました';
                }
            } 
        }
   
   
    }//ポスト処理終了
    //以降一覧商品取得
    $query = 'SELECT shop_item_table.item_id,name,price,img,status,stock FROM shop_item_table';
    $query .= ' JOIN shop_stock_table ON shop_item_table.item_id = shop_stock_table.item_id
            WHERE status = 2';
    $result = mysqli_query($link,$query);
    while($row = mysqli_fetch_array($result)) {
        $item_data[] = $row;
    }
    mysqli_free_result($result);
    mysqli_close($link);
} else {
    print 'DB';
}
?>



<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title>商品購入ページ</title>
    <link type="text/css" rel="stylesheet" href="./css/common.css">
    <style>
        #flex {
            width: 600px;
        }

        #flex .drink {
            
            /*swidth: 120px;*/
            height: 210px;
            text-align: center;
            margin: 10px;
            float: left; 
        }

        #flex span {
            display: block;
            margin: 3px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        
        .red {
            color: #FF0000;
        }

        #submit {
            clear: both;
        }
        header {
            height: 60px;
          　background-color: #0D2851;
        }
        .header-box {
            margin-left : auto ;
            margin-right : auto ;
            width: 960px;
        }
        .system_title {
            color: white;
            font-size:30px;
        }
    </style>
</head>

<body>
    <header>
        <div class="header-box">
            <span class="system_title">mamazon</span>
            <a class="nemu" href="./login.php?action_kind=logout">ログアウト</a>
            <a href="./cart.php" class="nemu">カートヘ行く</a>
        </div>
    </header>
       <div id="flex">
            <?php foreach($item_data as $value) { ?>  
                <div class="drink">
                    <span class="img_size"><img src="<?php print $folder.htmlspecialchars($value['img'],ENT_QUOTES,'UTF-8'); ?>"></span>
                    <span><?php print htmlspecialchars($value['name'],ENT_QUOTES,'UTF-8'); ?></span>
                    <span><?php print htmlspecialchars($value['price'],ENT_QUOTES,'UTF-8'); ?></span>
                    <?php if ($value['stock'] > 0) { ?>
    <form method="post">
        <input type="hidden" name="sql_kind" value="insert_cart">    
                        <input type="submit" value="カートへ入れる">
                        <input type="hidden" name="item_id" value="<?php print $value['item_id'] ; ?>">
    </form>
                        <?php } else { ?>
                            <span class="red">売り切れ</span>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
</body>
</html>