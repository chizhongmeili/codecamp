<?php
$host = 'localhost';
$username = 'root';
$passwd = '';
$dbname = 'codecamp';
$link = mysqli_connect($host,$username,$passwd,$dbname);
$cart_data = [];
$folder = './img_file/'; //後ろにファイル名がくるから最後の/がいる
$total = 0; //計算式の初期値は０
$err_msg = [];


session_start();
if (isset($_SESSION['user_id']) === false) {
    header('location:login.php');
    exit;
}
$user_id = $_SESSION['user_id'];

if ($link) {
  mysqli_set_charset($link,'utf8');
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $sql_kind = '';
    if (isset($_POST['sql_kind']) === TRUE){
      $sql_kind = $_POST['sql_kind'];
    }
    if($sql_kind === 'change_cart') {
       $item_id = '';
      if (isset($_POST['item_id']) === TRUE){
        $item_id = $_POST['item_id'];
      }
       $amount = '';
      if (isset($_POST['select_amount']) === TRUE){
        $amount = $_POST['select_amount'];
      }
      if ($amount === '') {
        $err_msg[] = '数量を入力してください';
      } else if (preg_match('/( |　)+/', $amount)) {
        $err_msg[] = '空白スペースは入力しないでください';
      } else if (preg_match('/^[1-9][0-9]*$/', $amount) !== 1) { //先頭は１～９いずれかひと文字
        $err_msg[] = '数量は1以上の整数値を入力してください';
      }
      if (count($err_msg) === 0) {
        $query = "UPDATE cart_table SET amount = {$amount}, updated_date = now() 
                WHERE user_id = {$user_id} AND item_id = {$item_id} ";
      
        if (mysqli_query($link,$query) === false) {
          $err_msg[] = '数量変更に失敗しました';
        }
      }
    } else if($sql_kind === 'delete_cart') {
       $item_id = '';
      if (isset($_POST['item_id']) === TRUE){
        $item_id = $_POST['item_id'];
      }
      
        $query = "DELETE FROM cart_table 
                WHERE user_id = {$user_id} AND item_id = {$item_id} ";
      
        if (mysqli_query($link,$query) === false) {
          $err_msg[] = '削除に失敗しました';
        }
    }
    
    
    
  }
  $query = 'SELECT user_id, cart_table.item_id, amount, shop_item_table.img, shop_item_table.name, shop_item_table.price
            FROM cart_table JOIN shop_item_table ON cart_table.item_id = shop_item_table.item_id
            WHERE user_id = ' . $user_id;
  $result = mysqli_query($link,$query);
  while($row = mysqli_fetch_array($result)) {
    $cart_data[] = $row;
    $total += $row['price'] * $row['amount'] ;
  }
}


?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>ショッピングカートページ</title>
  <link type="text/css" rel="stylesheet" href="./css/common.css">
<style>
  .system_title {
    color: white;
    font-size:30px;
    
  }
</style>
</head>
<body>
 <?php foreach($err_msg as $value) { ?>
 <p><?php print $value; ?></p>
  <?php } ?>
  <header>
    <div class="header-box">
      <a href="./index.php">
        <span class="system_title">mamazon</span>
      </a>
      <a class="nemu" href="./login.php?action_kind=logout">ログアウト</a>
      <a href="./index.php" class="nemu">商品一覧ページへ戻る</a>
    </div>
  </header>
  <div class="content">
    <h1 class="title">ショッピングカート</h1>

    <div class="cart-list-title">
      <span class="cart-list-price">価格</span>
      <span class="cart-list-num">数量</span>
    </div>
    <ul class="cart-list">
  <?php foreach($cart_data as $value) { ?>   
      <li>
        <div class="cart-item">
          <img class="cart-item-img" src="<?php print $folder.htmlspecialchars($value['img'],ENT_QUOTES,'UTF-8'); ?>">
          <span class="cart-item-name"><?php print htmlspecialchars($value['name'],ENT_QUOTES,'UTF-8'); ?></span>
          <form class="cart-item-del" action="./cart.php" method="post">
            <input type="submit" value="削除">
            <input type="hidden" name="item_id" value="<?php print htmlspecialchars($value['item_id'],ENT_QUOTES,'UTF-8'); ?>">
            <input type="hidden" name="sql_kind" value="delete_cart">
          </form>
          <span class="cart-item-price">¥ <?php print htmlspecialchars($value['price'],ENT_QUOTES,'UTF-8'); ?></span>
          <form class="form_select_amount" id="form_select_amount464" action="./cart.php" method="post">
            <input type="text" class="cart-item-num2" min="0" name="select_amount" value="<?php print htmlspecialchars($value['amount'],ENT_QUOTES,'UTF-8'); ?>">個&nbsp;<input type="submit" value="変更する">
            <input type="hidden" name="item_id" value="<?php print htmlspecialchars($value['item_id'],ENT_QUOTES,'UTF-8'); ?>">
            <input type="hidden" name="sql_kind" value="change_cart">
          </form>
        </div>
      </li>
    <?php } ?>
    </ul>
    <div class="buy-sum-box">
      <span class="buy-sum-title">合計</span>
      <span class="buy-sum-price">¥<?php print number_format($total) ;?></span>
    </div>
    <div>
      <form action="./result.php" method="post">
        <input class="buy-btn" type="submit" value="購入する">
      </form>
    </div>
  </div>
</body>
</html>
